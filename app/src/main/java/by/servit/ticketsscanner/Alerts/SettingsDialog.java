package by.servit.ticketsscanner.Alerts;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;

import by.servit.ticketsscanner.ChooseEventActivity;
import by.servit.ticketsscanner.Data.Settings;
import by.servit.ticketsscanner.R;

public class SettingsDialog {

    public AlertDialog mDialogBuilder;
    private SharedPreferences sp;
    private SharedPreferences.Editor ed;
    private CheckBox authorization;
    private CheckBox offline;
    private EditText countOfChars;
    private Switch users;
    private Switch events;
    private Switch tickets;
    private Switch passes;
    private Switch photo;

    public SettingsDialog(final ChooseEventActivity context){
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.settings, null);

        AlertDialog.Builder mDialogBuilderBuilder = new AlertDialog.Builder(context);

        mDialogBuilderBuilder.setView(promptsView);

        sp = context.getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);

        authorization = (CheckBox) promptsView.findViewById(R.id.authorization_checkbox);
        authorization.setChecked(sp.getBoolean(Settings.AUTHORIZATION, false));

        offline = (CheckBox) promptsView.findViewById(R.id.offline_checkbox);
        offline.setChecked(sp.getBoolean(Settings.OFFLINE, false));

        countOfChars = (EditText) promptsView.findViewById(R.id.count_of_chars_edittext);
        countOfChars.setText(String.valueOf(sp.getInt(Settings.COUNT_TICKETS_READS_CHARS, 13)));

        users = (Switch) promptsView.findViewById(R.id.download_users_switch);
        users.setChecked(sp.getBoolean(Settings.IS_DOWNLOAD_USERS, true));

        events = (Switch) promptsView.findViewById(R.id.download_all_events_switch);
        events.setChecked(sp.getBoolean(Settings.IS_DOWNLOAD_EVENTS, true));

        tickets = (Switch) promptsView.findViewById(R.id.download_tickets_switch);
        tickets.setChecked(sp.getBoolean(Settings.IS_DOWNLOAD_TICKETS, true));

        passes = (Switch) promptsView.findViewById(R.id.donload_upload_passes_switch);
        passes.setChecked(sp.getBoolean(Settings.IS_DOWNLOAD_PASSES, true));

        photo = (Switch) promptsView.findViewById(R.id.upload_photo_switch);
        photo.setChecked(sp.getBoolean(Settings.IS_UPLOAD_PHOTO, true));

        mDialogBuilderBuilder
                .setCancelable(false)
                .setPositiveButton("OK", null);

        mDialogBuilder = mDialogBuilderBuilder.create();

        mDialogBuilder.setOnShowListener(this.setListener(context));
    }

    private DialogInterface.OnShowListener setListener(final ChooseEventActivity context){
        return new DialogInterface.OnShowListener() {


            @Override
            public void onShow(DialogInterface dialogInterface) {

                final Intent intent = new Intent(context, ChooseEventActivity.class);

                Button button = mDialogBuilder.getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ed = sp.edit();
                        ed.putBoolean(Settings.AUTHORIZATION, authorization.isChecked());
                        ed.putBoolean(Settings.OFFLINE, offline.isChecked());
                        ed.putInt(Settings.COUNT_TICKETS_READS_CHARS, Integer.parseInt
                                (countOfChars.getText().toString()));
                        ed.putBoolean(Settings.IS_DOWNLOAD_USERS, users.isChecked());
                        ed.putBoolean(Settings.IS_DOWNLOAD_EVENTS, events.isChecked());
                        ed.putBoolean(Settings.IS_DOWNLOAD_PASSES, passes.isChecked());
                        ed.putBoolean(Settings.IS_DOWNLOAD_TICKETS, tickets.isChecked());
                        ed.putBoolean(Settings.IS_UPLOAD_PHOTO, photo.isChecked());
                        ed.commit();

                        Settings.setAuthorization(authorization.isChecked());
                        Settings.setOffline(offline.isChecked());
                        Settings.setCountOfReadsChars(Integer.parseInt
                                (countOfChars.getText().toString()));
                        Settings.setDownloadUsers(users.isChecked());
                        Settings.setDownloadEvents(events.isChecked());
                        Settings.setDownloadTickets(tickets.isChecked());
                        Settings.setDownloadPasses(passes.isChecked());
                        Settings.setUploadPhoto(photo.isChecked());

                        mDialogBuilder.dismiss();
                    }
                });
            }
        };
    }

}
