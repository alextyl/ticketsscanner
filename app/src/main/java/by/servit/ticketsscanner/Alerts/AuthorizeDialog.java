package by.servit.ticketsscanner.Alerts;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import by.servit.ticketsscanner.ChooseEventActivity;
import by.servit.ticketsscanner.Data.DBInstance.User;
import by.servit.ticketsscanner.Data.Settings;
import by.servit.ticketsscanner.LoginActivity;
import by.servit.ticketsscanner.R;

public class AuthorizeDialog {

    public AlertDialog mDialogBuilder;

    public AuthorizeDialog(final LoginActivity context, final String barcode){
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.prompt, null);

        AlertDialog.Builder mDialogBuilderBuilder = new AlertDialog.Builder(context);

        mDialogBuilderBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView.findViewById(R.id.input_text);


        mDialogBuilderBuilder
                .setCancelable(false)
                .setPositiveButton("OK", null)
                .setNegativeButton("Отмена",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                context.setNullBarcode();
                                dialog.cancel();
                            }
                        });

        mDialogBuilder = mDialogBuilderBuilder.create();

        mDialogBuilder.setOnShowListener(this.setListener(context, userInput, barcode));
    }

    private DialogInterface.OnShowListener setListener(final LoginActivity context,
                                                       final EditText userInput,
                                                       final String barcode){
        return new DialogInterface.OnShowListener() {

            SharedPreferences sp;
            SharedPreferences.Editor ed;

            @Override
            public void onShow(DialogInterface dialogInterface) {

                final Intent intent = new Intent(context, ChooseEventActivity.class);

                Button button = mDialogBuilder.getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(User.getInstance().Authorize(userInput.getText().toString(),
                                barcode, context)){
                            Toast.makeText(context, "Успешная авторизация", Toast.LENGTH_SHORT)
                                    .show();
                            context.setNullBarcode();

                            sp = context.getSharedPreferences("SETTINGS",Context.MODE_PRIVATE);
                            ed = sp.edit();
                            ed.putString(Settings.LOGIN, barcode);
                            ed.putString(Settings.PASSWORD, userInput.getText().toString());
                            ed.commit();

                            Settings.setLogin(barcode);
                            Settings.setPassword(userInput.getText().toString());

                            context.startActivity(intent);

                            mDialogBuilder.dismiss();
                        } else {
                            Toast.makeText(context, "Неуспешная авторизация", Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }
                });
            }
        };
    }

}

