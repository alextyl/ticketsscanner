package by.servit.ticketsscanner;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import by.servit.ticketsscanner.AsyncTasks.ReadTicket;
import by.servit.ticketsscanner.Data.DBHelper;
import by.servit.ticketsscanner.Data.DBInstance.Events;
import by.servit.ticketsscanner.Data.DBInstance.Passes;
import by.servit.ticketsscanner.Data.DBInstance.User;
import by.servit.ticketsscanner.Data.Settings;

public class TicketsScannerActivity extends AppCompatActivity implements SensorEventListener {

    private final static int MAKE_PHOTO = 100;

    private TextView mNameEventTextView;
    private TextView mStartEnterEventTextView;
    private TextView mEndEnterEventTextView;
    private TextView mZxingStatusTextView;
    private ListView mPassesListView;
    private ListView mLastPassesListView;
    private Button mPhotoButton;

    private DecoratedBarcodeView mBarcodeView;
    private BeepManager mBeepManager;
    private String mLastText;

    private boolean isLightOn = false;
    private static Handler mLightHandler;
    private static Handler mDeleteLastScanHandler;
    private ArrayList<String> mArrayList = new ArrayList<>();

    private File mDirectory;
    private String mLastPhotoPath;
    private String mLastPhotoUriPath;
    private String mLastPhotoName;


    private Thread deleteLastScan = new Thread(){
        @Override
        public void run() {
            super.run();
            while(!this.isInterrupted()){
                try {
                    sleep(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mDeleteLastScanHandler.sendEmptyMessage(0);
            }
        }
    };

    private boolean isAsyncTask = false;

    private SensorManager msensorManager;
    private float[] rotationMatrix;
    private float[] accelData;
    private float[] magnetData;
    private float[] OrientationData;

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            if(result.getText() == null || result.getText().equals(mLastText) || isAsyncTask
                    || result.getText().length() != Settings.getCountOfReadsChars()) {
                return;
            }

            mLastText = result.getText();
            mBarcodeView.setStatusText(result.getText());
            mBeepManager.playBeepSoundAndVibrate();
            new ReadTicket(TicketsScannerActivity.this).execute(mLastText);
        }



        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(android.R.style.Theme_Holo_NoActionBar);

        setContentView(R.layout.activity_tickets_scanner);

        makePhotoDir();

        mZxingStatusTextView = (TextView) findViewById(R.id.zxing_status_view);

        mPhotoButton = (Button) findViewById(R.id.photo_button);
        mPhotoButton.setOnClickListener(makePhoto);

        mNameEventTextView = (TextView) findViewById(R.id.name_textView);
        mNameEventTextView.setText(Events.getInstance().getEvents().
                get(Events.getInstance()
                        .getEventListIndexByEventId(Events.getInstance()
                                .getCurrentEventId())).getName());


        Date date = new Date(Events.getInstance().getCurrentEventDateStartEnter() * 1000 -
                (600*600 * 30));
        mStartEnterEventTextView = (TextView) findViewById(R.id.date_start_textView);
        mStartEnterEventTextView.setText(DBHelper.dateFormat.format(date));

        date = new Date(Events.getInstance().getCurrentgetDateEndEnter() * 1000 - (600*600 * 30));
        mEndEnterEventTextView = (TextView) findViewById(R.id.date_end_textView);
        mEndEnterEventTextView.setText(DBHelper.dateFormat.format(date));

        mBarcodeView = (DecoratedBarcodeView) findViewById(R.id.barcode_scanner);
        mBarcodeView.decodeContinuous(callback);
        mBeepManager = new BeepManager(this);

        //TODO: подумать над хендлерами

        mLightHandler = new Handler() {
            @Override
            public void handleMessage(android.os.Message msg) {
                changeLight();
            }
        };

        mDeleteLastScanHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                TicketsScannerActivity.this.setLastPassesList(null, false);
            }
        };

        mPassesListView = (ListView) findViewById(R.id.passes_list);
        mLastPassesListView = (ListView) findViewById(R.id.last_passes_list);

        deleteLastScan.start();

        msensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        rotationMatrix = new float[16];
        accelData = new float[3];
        magnetData = new float[3];
        OrientationData = new float[3];

    }

    @Override
    protected void onResume() {
        super.onResume();
        mBarcodeView.resume();
        msensorManager.registerListener(this, msensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI );
        msensorManager.registerListener(this, msensorManager
                .getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_UI );
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBarcodeView.pause();
        msensorManager.unregisterListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MAKE_PHOTO){
            if (resultCode == RESULT_OK){
                DBHelper dbHelper =  new DBHelper(this);
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                ContentValues contentValues = new ContentValues();
                contentValues.put("photo_name", mLastPhotoName);
                contentValues.put("employee_id", User.getInstance().getId());
                contentValues.put("event_id", Events.getInstance().getCurrentEventId());
                contentValues.put("photo_path", mLastPhotoPath);
                db.insert("photo", null, contentValues);
            }
        }
    }

    private View.OnClickListener makePhoto = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            Uri uri = Uri.fromFile(new File(mDirectory.getPath() + "/" + "photo_"
                    + System.currentTimeMillis() + ".jpg"));

            mLastPhotoPath = uri.getPath();
            mLastPhotoUriPath = uri.toString();
            mLastPhotoName = uri.getLastPathSegment();

            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(intent, MAKE_PHOTO);
        }
    };

    private void loadNewSensorData(SensorEvent event) {
        final int type = event.sensor.getType();
        if (type == Sensor.TYPE_ACCELEROMETER) {
            accelData = event.values.clone();
        }

        if (type == Sensor.TYPE_MAGNETIC_FIELD) {
            magnetData = event.values.clone();
        }
    }

    public void setStatusTextColorGood(){
        mZxingStatusTextView.setTextColor(Color.GREEN);
    }

    public void setStatusTextColorBad(){
        mZxingStatusTextView.setTextColor(Color.RED);
    }

    public void setStatusTextColorRepeat(){
        mZxingStatusTextView.setTextColor(Color.YELLOW);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                this.setLightOn(false);
                break;
            case KeyEvent.KEYCODE_VOLUME_UP:
                this.setLightOn(true);
                break;
        }
        return mBarcodeView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    public void changeLight(){
        if(!isLightOn){
            mBarcodeView.setTorchOn();
        } else {
            mBarcodeView.setTorchOff();
        }
        TicketsScannerActivity.this.setLightOn(!isLightOn);
    }

    public void stroboscope(final long wait, final int strobCount) {
        Thread thread = new Thread(){
        @Override
        public void run() {
            int strobCounter =  strobCount * 2;
            while (strobCounter > 0){
                mLightHandler.sendEmptyMessage(0);
                try {
                    Thread.sleep(wait);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                strobCounter--;
            }
        }};

        thread.start();
    }

    public synchronized void playSound(int soundID){
        MediaPlayer mp = MediaPlayer.create(TicketsScannerActivity.this, soundID);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.stop();
                mp.release();
            }
        });
        mp.start();
    }

    public void setPassesList(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, Passes.getInstance().getPassesToList());
        mPassesListView.setAdapter(adapter);
    }

    public void setLastPassesList(String string, boolean add){
        if(add){
            mArrayList.add(0, string);
            if(mArrayList.size() > 4){
                mArrayList.remove(mArrayList.size() -1);
            }
        } else {
            if(mArrayList.size() > 0){
                mArrayList.remove(mArrayList.size() -1);
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.list_item_custom, mArrayList);
        mLastPassesListView.setAdapter(adapter);
    }

    public void makePhotoDir(){

        mDirectory = new File(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "TicketsScanner");

        if (!mDirectory.exists()){
            if(!mDirectory.mkdirs()){
                Toast.makeText(getApplicationContext(),
                        "Невозможно создать директорию для сохранения фото",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    public boolean isAsyncTask() {
        return isAsyncTask;
    }

    public void setAsyncTask(boolean asyncTask) {
        isAsyncTask = asyncTask;
    }

    public DecoratedBarcodeView getBarcodeView() {
        return mBarcodeView;
    }

    public String getLastText() {
        return mLastText;
    }

    public void setLightOn(boolean lightOn) {
        isLightOn = lightOn;
    }

    public ListView getLastPassesListView() {
        return mLastPassesListView;
    }

    public void setLastPassesListView(ListView lastPassesListView) {
        mLastPassesListView = lastPassesListView;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        loadNewSensorData(sensorEvent);
        SensorManager.getRotationMatrix(rotationMatrix, null, accelData, magnetData);
        SensorManager.getOrientation(rotationMatrix, OrientationData);

        if(Math.round(Math.toDegrees(OrientationData[1])) >= 0){
            WindowManager.LayoutParams params = getWindow().getAttributes();
            params.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
            params.screenBrightness = 0;
            getWindow().setAttributes(params);
        } else {
            WindowManager.LayoutParams params = getWindow().getAttributes();
            params.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
            params.screenBrightness = -1;
            getWindow().setAttributes(params);
        }
    }

    public Button getPhotoButton() {
        return mPhotoButton;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
