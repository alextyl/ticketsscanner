package by.servit.ticketsscanner;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.net.Uri;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.File;
import java.io.IOException;
import java.security.Permission;
import java.util.Set;

import by.servit.ticketsscanner.Alerts.AuthorizeDialog;
import by.servit.ticketsscanner.AsyncTasks.CreateDB;
import by.servit.ticketsscanner.AsyncTasks.FullSynchronization;
import by.servit.ticketsscanner.Data.DBInstance.User;
import by.servit.ticketsscanner.Data.Settings;

public class LoginActivity extends AppCompatActivity {

    private Button mLoginButton;
    private Button mSynchronizationButton;
    private ProgressBar mUpdateProgressBar;
    private TextView mUpdateTextView;

    private String mUserBarcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(android.R.style.Theme_Holo_NoActionBar);

        setContentView(R.layout.activity_login);

        mLoginButton = (Button) findViewById(R.id.login_button);
        mLoginButton.setOnClickListener(login);

        mSynchronizationButton = (Button) findViewById(R.id.synchronization_button);
        mSynchronizationButton.setOnClickListener(synchronization);

        mUpdateProgressBar = (ProgressBar) findViewById(R.id.update_db_progressBar);
        mUpdateTextView = (TextView) findViewById(R.id.update_db_textView);

        setSettings();

        if(Settings.isAuthorization() &&
                User.getInstance().Authorize(Settings.getPassword(), Settings.getLogin(), this)){
            Intent intent = new Intent(this, ChooseEventActivity.class);
            this.startActivity(intent);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private View.OnClickListener login = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mUserBarcode = null;
            IntentIntegrator integrator = new IntentIntegrator(LoginActivity.this);
            integrator.setBeepEnabled(true);
            integrator.setOrientationLocked(true);
            integrator.setPrompt(getString(R.string.login_scan_promt_auth));
            integrator.initiateScan();
        }
    };

    private View.OnClickListener synchronization = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new CreateDB(LoginActivity.this).execute();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null && result.getContents() != null) {
            Toast.makeText(getApplicationContext(),
                    getText(R.string.scan_result) + result.getContents(), Toast.LENGTH_SHORT).show();
            mUserBarcode = result.getContents();
            if(!User.getInstance().getAuthorized() && mUserBarcode != null){
                AlertDialog alertDialog =
                        new  AuthorizeDialog(this, mUserBarcode).mDialogBuilder;
                alertDialog.show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void setSettings(){
        SharedPreferences sp = getSharedPreferences("SETTINGS",MODE_PRIVATE);
        Settings.setAuthorization(sp.getBoolean(Settings.AUTHORIZATION, true));
        Settings.setLogin(sp.getString(Settings.LOGIN, ""));
        Settings.setPassword(sp.getString(Settings.PASSWORD, ""));
        Settings.setOffline(sp.getBoolean(Settings.OFFLINE, false));
        Settings.setCountOfReadsChars(sp.getInt(Settings.COUNT_TICKETS_READS_CHARS, 13));
        Settings.setDownloadUsers(sp.getBoolean(Settings.IS_DOWNLOAD_USERS, true));
        Settings.setDownloadEvents(sp.getBoolean(Settings.IS_DOWNLOAD_EVENTS, true));
        Settings.setDownloadTickets(sp.getBoolean(Settings.IS_DOWNLOAD_TICKETS, true));
        Settings.setDownloadPasses(sp.getBoolean(Settings.IS_DOWNLOAD_PASSES, true));
        Settings.setUploadPhoto(sp.getBoolean(Settings.IS_UPLOAD_PHOTO, true));
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
        } else {
            TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            Settings.setIMEI(telephonyManager.getDeviceId());
            new CreateDB(this).execute();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
                    Settings.setIMEI(telephonyManager.getDeviceId());
                    new CreateDB(this).execute();
                } else {
                  Toast.makeText(getApplicationContext(),
                          "Ошибка разрешения прав! Невозможно определить IMEI!",
                          Toast.LENGTH_LONG).show();
                }
            }


        }

    }

    public void visibilityUpdateDB(boolean isUpdate){
        if(isUpdate){
            mLoginButton.setVisibility(View.GONE);
            mUpdateProgressBar.setVisibility(View.VISIBLE);
            mUpdateTextView.setVisibility(View.VISIBLE);
        } else {
            mLoginButton.setVisibility(View.VISIBLE);
            mUpdateProgressBar.setVisibility(View.GONE);
            mUpdateTextView.setVisibility(View.GONE);
        }
    }

    public void setNullBarcode(){
        this.mUserBarcode = null;
    }

}
