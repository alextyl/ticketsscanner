package by.servit.ticketsscanner.Data.DBEssence;

import android.database.Cursor;

import by.servit.ticketsscanner.Data.DBHelper;


public class Event {

    private static final int DB_ID = 0;
    private static final int DB_NAME = 1;
    private static final int DB_DATE_EVENT = 2;
    private static final int DB_DATE_START_ENTER = 3;
    private static final int DB_DATE_END_ENTER = 4;
    private static final int DB_PLATFORM_ID = 5;
    private static final int DB_ORGANIZER_ID = 6;
    private static final int DB_CAN_CHOOSE = 7;

    private int mId;
    private String mName;
    private long mDateEvent;
    private long mDateStartEnter;
    private long mDateEndEnter;
    private int mPlatformId;
    private int mOrganizerId;
    private boolean mIsCanChoose;

    public Event(Cursor cursor, int index){
        cursor.moveToPosition(index);
        mId = cursor.getInt(DB_ID);
        mName = cursor.getString(DB_NAME);
        mDateEvent = cursor.getLong(DB_DATE_EVENT);
        mDateStartEnter = cursor.getLong(DB_DATE_START_ENTER);
        mDateEndEnter = cursor.getLong(DB_DATE_END_ENTER);
        mPlatformId = cursor.getInt(DB_PLATFORM_ID);
        mOrganizerId = cursor.getInt(DB_ORGANIZER_ID);
        mIsCanChoose = DBHelper.getBoolean(cursor, DB_CAN_CHOOSE);
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public long getDateEvent() {
        return mDateEvent;
    }

    public void setDateEvent(long dateEvent) {
        mDateEvent = dateEvent;
    }

    public long getDateStartEnter() {
        return mDateStartEnter;
    }

    public void setDateStartEnter(long dateStartEnter) {
        mDateStartEnter = dateStartEnter;
    }

    public long getDateEndEnter() {
        return mDateEndEnter;
    }

    public void setDateEndEnter(long dateEndEnter) {
        mDateEndEnter = dateEndEnter;
    }

    public int getPlatformId() {
        return mPlatformId;
    }

    public void setPlatformId(int platformId) {
        mPlatformId = platformId;
    }

    public int getOrganizerId() {
        return mOrganizerId;
    }

    public void setOrganizerId(int organizerId) {
        mOrganizerId = organizerId;
    }

    public boolean isCanChoose() {
        return mIsCanChoose;
    }

    public void setCanChoose(boolean canChoose) {
        mIsCanChoose = canChoose;
    }
}
