package by.servit.ticketsscanner.Data;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class DBHelper extends SQLiteOpenHelper {
    // TODO: Сделать доступ через инстанс
    public final static String DB_NAME = "ticketsScannerDB";

    private final static String[] SQL_TABLES = {
            "CREATE TABLE \"device\" " +
                    "( `_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "`name` TEXT, " +
                    "`IMEI` TEXT, " +
                    "`active` INTEGER, " +
                    "`online` INTEGER, " +
                    "`last_date` TEXT, " +
                    "`employee_id` INTEGER, " +
                    "`event_id` INTEGER, " +
                    "`entrance_id` INTEGER, " +
                    "`platform_id` INTEGER, " +
                    "FOREIGN KEY(`event_id`) REFERENCES `event`(`_id`), " +
                    "FOREIGN KEY(`employee_id`) REFERENCES `employee`(`_id`), " +
                    "FOREIGN KEY(`platform_id`) REFERENCES `platform`(`_id`)," +
                    " FOREIGN KEY(`entrance_id`) REFERENCES `entrance`(`_id`) );",

            "CREATE TABLE \"employee\" " +
                    "( `_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "`full_name` TEXT, " +
                    "`active` INTEGER," +
                    " `allowed_outputs` INTEGER," +
                    " `type` TEXT, " +
                    "`login` TEXT," +
                    " `password` TEXT, " +
                    "`operator_id` INTEGER," +
                    " `organizer_id` INTEGER," +
                    " `platform_id` INTEGER," +
                    " FOREIGN KEY(`platform_id`) REFERENCES `platform`(`_id`)," +
                    " FOREIGN KEY(`organizer_id`) REFERENCES `organizer`(`_id`)," +
                    " FOREIGN KEY(`operator_id`) REFERENCES `operator`(`_id`) );",

            "CREATE TABLE \"entrance\"" +
                    " ( `_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " `platform_id` INTEGER," +
                    " `name` TEXT," +
                    " `active` INTEGER," +
                    " `coordinate_x` REAL," +
                    " `coordinate_y` REAL," +
                    " `can_exit` INTEGER," +
                    " FOREIGN KEY(`platform_id`) REFERENCES `platform`(`_id`) );",

            "CREATE TABLE `event` " +
                    "( `_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " `name` TEXT," +
                    " `date_event` TEXT," +
                    " `date_start_enter` TEXT," +
                    " `date_end_enter` TEXT, " +
                    "`platform_id` INTEGER," +
                    " `organizer_id` INTEGER, " +
                    "`can_choose` INTEGER," +
                    " FOREIGN KEY(`platform_id`) REFERENCES `platform`(`_id`)," +
                    " FOREIGN KEY(`organizer_id`) REFERENCES `organizer`(`_id`) );",

            "CREATE TABLE `operator` " +
                    "( `_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " `name` TEXT," +
                    " `barcode_length` INTEGER," +
                    " `barcode_type` TEXT," +
                    " `ftp_login` TEXT," +
                    " `ftp_password` TEXT );",

            "CREATE TABLE `organizer` " +
                    "( `_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " `name` TEXT," +
                    " `active` INTEGER );",

            "CREATE TABLE \"pass\" " +
                    "( `_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " `device_id` INTEGER," +
                    " `ticket_id` INTEGER," +
                    " `employee_id` INTEGER," +
                    " `event_id` INTEGER," +
                    " `entry_date` INTEGER," +
                    " `direction` NUMERIC," +
                    " `result` TEXT," +
                    " `inServer` INTEGER," +
                    " FOREIGN KEY(`employee_id`) REFERENCES `employee`(`_id`)," +
                    " FOREIGN KEY(`device_id`) REFERENCES `device`(`_id`) );",

            "CREATE TABLE `photo` " +
                    "( `_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "`photo_name` TEXT," +
                    " `entrance_id` INTEGER," +
                    " `photo_path` TEXT," +
                    " `employee_id` INTEGER," +
                    " `event_id` INTEGER," +
                    " FOREIGN KEY(`employee_id`) REFERENCES `employee`(`_id`) );",

            "CREATE TABLE `platform` " +
                    "( `_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "`name` TEXT," +
                    " `admin` INTEGER," +
                    " `outputs_allowed` INTEGER," +
                    " `image` TEXT," +
                    " FOREIGN KEY(`admin`) REFERENCES `employee`(`_id`) );",

            "CREATE TABLE \"ticket\"" +
                    " ( `_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " `date_upload` TEXT," +
                    " `operator_id` INTEGER," +
                    " `event_id` INTEGER, " +
                    "`barcode` TEXT," +
                    " `sector` TEXT," +
                    " `row` INTEGER," +
                    " `place` INTEGER," +
                    " `no_place` INTEGER," +
                    " `active` INTEGER," +
                    " `transactional` INTEGER," +
                    " FOREIGN KEY(`event_id`) REFERENCES `event`(`_id`)," +
                    " FOREIGN KEY(`operator_id`) REFERENCES `operator`(`_id`) );",

            "CREATE TABLE \"bad_pass\"" +
                    " ( `_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " `date` TEXT," +
                    " `event_id` INTEGER, " +
                    "`barcode` TEXT," +
                    "`employee_id` INTEGER);"
    };

    public final static String WHERE_LOGIN_PASSWORD = "login = ? AND password = ?";
    public final static String WHERE_PHOTO_PATH = "photo_path = ?";
    public final static String WHERE_DATE_CURRENT = "date_event > ? - 60*60*15 ";
    public final static String WHERE_BARCODE_EVENT = "barcode = ? AND event_id = ? AND active = 1";
    public final static String WHERE_EVENT_ID = "event_id = ?";
    public final static String WHERE_TICKET_EVENT = "ticket_id = ? AND event_id = ?";
    public final static String WHERE_NOT_IN_SERVER = "inServer = 0";
    public final static String WHERE_ID = "_id = ?";

    private final static int VERSION = 7;

    public final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    public final static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

    private final static SQLiteDatabase.CursorFactory sCursorFactory = null;

    public DBHelper(Context context){
        super(context, DB_NAME, sCursorFactory, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        for (String sqlQuery : SQL_TABLES){
            sqLiteDatabase.execSQL(sqlQuery);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(            "CREATE TABLE `photo` " +
                "( `_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "`photo_name` TEXT," +
                " `entrance_id` INTEGER," +
                " `photo_path` TEXT," +
                " `employee_id` INTEGER," +
                " `event_id` INTEGER," +
                " FOREIGN KEY(`employee_id`) REFERENCES `employee`(`_id`) );");
    }

    public static boolean getBoolean(Cursor cursor, int columnId){
       return cursor.getInt(columnId) == 1;
    }


}
