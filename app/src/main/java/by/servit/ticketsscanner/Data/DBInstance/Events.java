package by.servit.ticketsscanner.Data.DBInstance;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import by.servit.ticketsscanner.Data.DBEssence.Event;
import by.servit.ticketsscanner.Data.DBHelper;

import static by.servit.ticketsscanner.Data.DBHelper.WHERE_DATE_CURRENT;


public final class Events {

    private static final Events INSTANCE = new Events();

    private Events(){}

    public static Events getInstance(){
        return INSTANCE;
    }

    private static ArrayList<Event> mEvents;
    private static int currentEventId;

    public ArrayList<Event> getmEvents() {
        return mEvents;
    }

    public void uploadEvents(Context context){

        mEvents = new ArrayList<>();

        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        String[] selectionArgs = new String[] { String.valueOf(timestamp.getTime() / 1000) };
        Cursor cursor = db.query("event", null, WHERE_DATE_CURRENT, selectionArgs,
                null, null, null);


        int count = cursor.getCount();

        if(count > 0){
            for(int i = 0; i < count; i++){
                mEvents.add(new Event(cursor, i));
            }
        }

        cursor.close();
        db.close();
    }

    public  ArrayList<Event> getEvents() {
        return mEvents;
    }

    public void setEvents(ArrayList<Event> mEvents) {
        Events.mEvents = mEvents;
    }

    public String[] getEventsNames(){
        String[] eventsNames = new String[mEvents.size()];
        for(int i = 0; i < mEvents.size(); i++){
            Date date = new Date(mEvents.get(i).getDateStartEnter() * 1000 -
                    (600*600 * 30));
            eventsNames[i] = mEvents.get(i).getName() + "\n " + DBHelper.dateFormat.format(date);
        }
        return eventsNames;
    }

    public int getEventListIndexByEventId(int id){
        int index = 0;

        for(int i = 0; i < this.getmEvents().size(); i++){
            if(this.getmEvents().get(i).getId() == id){
                index = i;
            }
        }

        return index;
    }

    public long getCurrentEventDateStartEnter(){
        return Events.getInstance().getEvents().get(Events.getInstance()
                .getEventListIndexByEventId(Events.getInstance()
                        .getCurrentEventId())).getDateStartEnter();
    }

    public long getCurrentgetDateEndEnter(){
        return Events.getInstance().getEvents().get(Events.getInstance()
                .getEventListIndexByEventId(Events.getInstance()
                        .getCurrentEventId())).getDateEndEnter();
    }

    public  int getCurrentEventId() {
        return currentEventId;
    }

    public  void setCurrentEventId(int currentEventId) {
        Events.currentEventId = currentEventId;
    }
}
