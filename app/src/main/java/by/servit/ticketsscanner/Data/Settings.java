package by.servit.ticketsscanner.Data;


public class Settings {

    public static final String AUTHORIZATION = "auto";
    public static final String LOGIN = "log";
    public static final String PASSWORD = "pass";
    public static final String OFFLINE = "off";
    public static final String COUNT_TICKETS_READS_CHARS = "count";
    public static final String IS_DOWNLOAD_USERS = "down_user";
    public static final String IS_DOWNLOAD_EVENTS = "down_events";
    public static final String IS_DOWNLOAD_TICKETS = "down_tickets";
    public static final String IS_UPLOAD_PHOTO = "up_photo";
    public static final String IS_DOWNLOAD_PASSES = "down_pass";
    public static final String IMEI = "imei";


    private static boolean sAuthorization;
    private static String sLogin;
    private static String sPassword;
    private static boolean sOffline;
    private static int sCountOfReadsChars;
    private static boolean sDownloadUsers;
    private static boolean sDownloadTickets;
    private static boolean sDownloadEvents;
    private static boolean sUploadPhoto;
    private static boolean sDownloadPasses;
    private static String sIMEI;

    public static boolean isDownloadUsers() {
        return sDownloadUsers;
    }

    public static void setDownloadUsers(boolean downloadUsers) {
        sDownloadUsers = downloadUsers;
    }

    public static boolean isDownloadTickets() {
        return sDownloadTickets;
    }

    public static void setDownloadTickets(boolean downloadTickets) {
        sDownloadTickets = downloadTickets;
    }

    public static boolean isDownloadEvents() {
        return sDownloadEvents;
    }

    public static void setDownloadEvents(boolean downloadEvents) {
        sDownloadEvents = downloadEvents;
    }

    public static boolean isUploadPhoto() {
        return sUploadPhoto;
    }

    public static void setUploadPhoto(boolean uploadPhoto) {
        sUploadPhoto = uploadPhoto;
    }

    public static boolean isDownloadPasses() {
        return sDownloadPasses;
    }

    public static void setDownloadPasses(boolean downloadPasses) {
        sDownloadPasses = downloadPasses;
    }

    public static boolean isAuthorization() {
        return sAuthorization;
    }

    public static void setAuthorization(boolean authorization) {
        sAuthorization = authorization;
    }

    public static String getLogin() {
        return sLogin;
    }

    public static void setLogin(String login) {
        sLogin = login;
    }

    public static String getPassword() {
        return sPassword;
    }

    public static void setPassword(String password) {
        sPassword = password;
    }

    public static boolean isOffline() {
        return sOffline;
    }

    public static void setOffline(boolean offline) {
        sOffline = offline;
    }

    public static int getCountOfReadsChars() {
        return sCountOfReadsChars;
    }

    public static void setCountOfReadsChars(int countOfReadsChars) {
        sCountOfReadsChars = countOfReadsChars;
    }

    public static boolean checkSynchronizationSettings(){
        return !isDownloadUsers() && !isDownloadEvents() && !isDownloadTickets()
                && !isDownloadPasses()
                && !isUploadPhoto();
    }

    public static String getIMEI() {
        return sIMEI;
    }

    public static void setIMEI(String IMEI) {
        sIMEI = IMEI;
    }
}
