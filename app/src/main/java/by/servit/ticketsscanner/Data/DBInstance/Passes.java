package by.servit.ticketsscanner.Data.DBInstance;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;

import by.servit.ticketsscanner.Data.DBEssence.Pass;
import by.servit.ticketsscanner.Data.DBHelper;

import static by.servit.ticketsscanner.Data.DBHelper.WHERE_TICKET_EVENT;


public class Passes {

    private static final Passes INSTANCE = new Passes();

    private Passes(){}

    public static Passes getInstance(){
        return INSTANCE;
    }

    private static ArrayList<Pass> mPasses;

    public void uploadPasses(Context context, int ticketId, int eventId){

        mPasses = new ArrayList<>();

        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String[] selectionArgs = new String[] { String.valueOf(ticketId), String.valueOf(eventId) };
        Cursor cursor = db.query("pass", null, WHERE_TICKET_EVENT, selectionArgs,
                null, null, null);


        int count = cursor.getCount();

        if(count > 0){
            for(int i = 0; i < count; i++){
                mPasses.add(new Pass(cursor, i));
            }
        }

        cursor.close();
        db.close();
        Log.d("Passes", mPasses.toString());
    }

    public String[] getPassesToList(){
        String[] passes = new String[mPasses.size()];
        for(int i = 0; i < mPasses.size(); i++){
            passes[i] = DBHelper.dateFormat.format(new Date(mPasses.get(i).getEntryDate() * 1000))
                    + " " + (mPasses.get(i).isDirection()?  "Вход": "Выход");
        }
        return passes;
    }

    public int getRetryPass(){
        return this.getPasses().size();
    }

    public ArrayList<Pass> getPasses() {
        return mPasses;
    }

    public void setPasses(ArrayList<Pass> mPasses) {
        Passes.mPasses = mPasses;
    }

}
