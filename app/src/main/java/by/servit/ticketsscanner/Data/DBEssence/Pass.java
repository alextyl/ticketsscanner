package by.servit.ticketsscanner.Data.DBEssence;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import by.servit.ticketsscanner.Data.DBHelper;

import static by.servit.ticketsscanner.Data.DBHelper.WHERE_TICKET_EVENT;

public class Pass {

    public static final int ON_SERVER = 1;
    public static final int NOT_ON_SERVER = 0;

    public static final int DB_ID = 0;
    public static final int DB_DEVICE_ID = 1;
    public static final int DB_TICKET_ID = 2;
    public static final int DB_EMPLOYEE_ID = 3;
    public static final int DB_EVENT_ID = 4;
    public static final int DB_ENTRY_DATE = 5;
    public static final int DB_DIRECTION = 6;
    public static final int DB_RESULT = 7;
    public static final int DB_ISSERVER = 8;

    public static final int DB_BAD_ID = 0;
    public static final int DB_BAD_DATE = 1;
    public static final int DB_BAD_EVENT_ID = 2;
    public static final int DB_BAD_BARCODE = 3;
    public static final int DB_BAD_EMPLOYEE_ID = 4;

    private int mId;
    private int mDeviceId;
    private int mTicketId;
    private int mEmployeeId;
    private int mEventId;
    private long mEntryDate;
    private boolean mDirection;
    private String mResult;
    private boolean mIsServer;


    public Pass(Cursor cursor, int index){
        cursor.moveToPosition(index);
        mId = cursor.getInt(DB_ID);
        mDeviceId = cursor.getInt(DB_DEVICE_ID);
        mTicketId = cursor.getInt(DB_TICKET_ID);
        mTicketId = cursor.getInt(DB_EMPLOYEE_ID);
        mEventId = cursor.getInt(DB_EVENT_ID);
        mEntryDate = cursor.getLong(DB_ENTRY_DATE);
        mDirection = DBHelper.getBoolean(cursor, DB_DIRECTION);
        mResult = cursor.getString(DB_RESULT);
        mIsServer = DBHelper.getBoolean(cursor, DB_ISSERVER);
    }

    public static int checkDirection(int ticketId, int eventId, Context context){
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String[] selectionArgs = new String[] { String.valueOf(ticketId), String.valueOf(eventId) };
        Cursor cursor = db.query("pass", null, WHERE_TICKET_EVENT, selectionArgs,
                null, null, null);

        int result = (cursor.getCount() + 1) % 2;

        cursor.close();

        return result;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getDeviceId() {
        return mDeviceId;
    }

    public void setDeviceId(int deviceId) {
        mDeviceId = deviceId;
    }

    public int getTicketId() {
        return mTicketId;
    }

    public void setTicketId(int ticketId) {
        mTicketId = ticketId;
    }

    public int getEmployeeId() {
        return mEmployeeId;
    }

    public void setEmployeeId(int employeeId) {
        mEmployeeId = employeeId;
    }

    public int getEventId() {
        return mEventId;
    }

    public void setEventId(int eventId) {
        mEventId = eventId;
    }

    public long getEntryDate() {
        return mEntryDate;
    }

    public void setEntryDate(long entryDate) {
        mEntryDate = entryDate;
    }

    public boolean isDirection() {
        return mDirection;
    }

    public void setDirection(boolean direction) {
        mDirection = direction;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

    @Override
    public String toString(){
        return String.valueOf(this.getId()) + " " +
                String.valueOf(this.getDeviceId()) + " " +
                String.valueOf(this.getTicketId()) + " " +
                String.valueOf(this.getEmployeeId()) + " " +
                String.valueOf(this.getEventId()) + " " +
                String.valueOf(this.getEntryDate()) + " " +
                String.valueOf(this.isDirection()) + " " +
                String.valueOf(this.getResult());
    }

}
