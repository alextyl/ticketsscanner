package by.servit.ticketsscanner.Data.DBInstance;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import by.servit.ticketsscanner.Data.DBHelper;

import static by.servit.ticketsscanner.Data.DBHelper.WHERE_LOGIN_PASSWORD;

public final class User {

    private static final int DB_ID = 0;
    private static final int DB_FULL_NAME = 1;
    private static final int DB_ACTIVE = 2;
    private static final int DB_ALLOWED_OUTPUTS = 3;
    private static final int DB_TYPE = 4;
    private static final int DB_LOGIN = 5;
    private static final int DB_PASSWORD = 6;
    private static final int DB_OPERATOR_ID = 7;
    private static final int DB_ORGANIZER_ID = 8;
    private static final int DB_PLATFORM_ID = 9;

    private static final User INSTANCE = new User();

    private User(){}

    public static User getInstance(){
        return INSTANCE;
    }

    private String mId;
    private String mFullName;
    private String mType;
    private String mLogin;
    private String mPassword;
    private int mOperatorId;
    private int mOrganizerId;
    private int mPlatformId;
    private boolean mAllowedOutputs;
    private boolean mActive;

    private boolean mAuthorized = false;

    public Boolean getAuthorized() {
        return mAuthorized;
    }

    public boolean Authorize(String password, String barcode, Context context){
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String[] selectionArgs = new String[] { barcode, password };
        Cursor cursor = db.query("employee", null, WHERE_LOGIN_PASSWORD, selectionArgs,
                null, null, null);
        if(cursor.getCount() > 0){
            cursor.moveToFirst();
            mId = cursor.getString(DB_ID);
            mFullName = cursor.getString(DB_FULL_NAME);
            mType = cursor.getString(DB_TYPE);
            mLogin = cursor.getString(DB_LOGIN);
            mPassword = cursor.getString(DB_PASSWORD);
            mOperatorId = cursor.getInt(DB_OPERATOR_ID);
            mOrganizerId = cursor.getInt(DB_ORGANIZER_ID);
            mPlatformId = cursor.getInt(DB_PLATFORM_ID);
            mAllowedOutputs = DBHelper.getBoolean(cursor, DB_ALLOWED_OUTPUTS);
            mActive = DBHelper.getBoolean(cursor, DB_ACTIVE);
            cursor.close();
            db.close();
            return true;
        }
        db.close();
        return false;
    }


    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getLogin() {
        return mLogin;
    }

    public void setLogin(String login) {
        mLogin = login;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public int getOperatorId() {
        return mOperatorId;
    }

    public void setOperatorId(int operatorId) {
        mOperatorId = operatorId;
    }

    public int getOrganizerId() {
        return mOrganizerId;
    }

    public void setOrganizerId(int organizerId) {
        mOrganizerId = organizerId;
    }

    public int getPlatformId() {
        return mPlatformId;
    }

    public void setPlatformId(int platformId) {
        mPlatformId = platformId;
    }

    public boolean isAllowedOutputs() {
        return mAllowedOutputs;
    }

    public void setAllowedOutputs(boolean allowedOutputs) {
        mAllowedOutputs = allowedOutputs;
    }

    public boolean isActive() {
        return mActive;
    }

    public void setActive(boolean active) {
        mActive = active;
    }
}
