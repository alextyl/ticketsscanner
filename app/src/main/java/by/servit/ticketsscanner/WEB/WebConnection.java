package by.servit.ticketsscanner.WEB;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import by.servit.ticketsscanner.Data.DBHelper;
import by.servit.ticketsscanner.Data.Settings;

import static by.servit.ticketsscanner.Data.DBHelper.WHERE_PHOTO_PATH;

public class WebConnection {

    private static final int TIMEOUT_TIME = 10000;
    public static final String HOST = "http://213.184.248.201/include/BarcodeScanner";

    private static final String lineEnd = "\r\n";
    private static final String twoHyphens = "--";
    private static final String boundary = "*****";
    private static final int maxBufferSize = 1 * 1024 * 1024;

    private JSONObject mJSONObject;
    private JSONArray mJSONArray;
    private URL mURL;

    public WebConnection(String url) throws MalformedURLException {
        mURL = new URL(url);
        mJSONObject = new JSONObject();
    }

    public WebConnection(String url, JSONObject jsonObject) throws MalformedURLException {
        mURL = new URL(url);
        mJSONObject = jsonObject;
    }

    public WebConnection(String url, JSONArray jsonArray) throws MalformedURLException {
        mURL = new URL(url);
        mJSONArray = jsonArray;
    }

    public String getResponse(){

        Log.d("WebConnection", this.mURL.toString());

        try {
            if(mJSONArray != null){
                JSONObject object = new JSONObject();
                object.put("login", Settings.getLogin());
                object.put("password", Settings.getPassword());
                object.put("imei", Settings.getIMEI());
                mJSONArray.put(object);
                Log.d("WebConnection", "JSONArray: " + mJSONArray.toString());
            } else {
                mJSONObject.put("login", Settings.getLogin());
                mJSONObject.put("password", Settings.getPassword());
                mJSONObject.put("imei", Settings.getIMEI());
                Log.d("WebConnection", "JSONObject: " + mJSONObject.toString());
            }

        } catch (Exception e){
            return null;
        }

        String response = "";

        try {
            HttpURLConnection connection = (HttpURLConnection) mURL.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setReadTimeout(TIMEOUT_TIME);
            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            connection.setRequestProperty("Accept","application/json");
            DataOutputStream os = new DataOutputStream(connection.getOutputStream());
            if(mJSONObject != null){
                os.writeBytes(mJSONObject.toString());
            } else if (mJSONArray != null){
                os.writeBytes(mJSONArray.toString());
            }
            os.flush();
            os.close();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String s;
            while((s = br.readLine()) != null){
                response += s;
            }
            connection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        Log.d("WebConnection", "response: " + response);
        return response;
    }

    public static String uploadPhotos(Context context) throws IOException {
        DBHelper dbHelper =  new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor cursor = db.query("photo", null, null, null, null, null, null , null);
        cursor.moveToFirst();

        URL url = new URL(HOST + "/uploadPhoto.php");
        Log.d("WebConnection", url.toString());
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Connection", "Keep-Alive");
        conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
        DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
        dos.writeBytes(twoHyphens + boundary);

        for (int i = 0; !cursor.isAfterLast(); cursor.moveToNext()){

            int photoId = cursor.getInt(0);
            String photoName = cursor.getString(1);
            int entranceId = cursor.getInt(2);
            String photoPath = cursor.getString(3);
            int employeeId = cursor.getInt(4);
            int eventId = cursor.getInt(5);



            dos.writeBytes(lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\""+
                    photoName + "\";filename=\"" + photoName + "\"" + lineEnd);
            dos.writeBytes("Content-Type: image/jpeg" + lineEnd);
            dos.writeBytes(lineEnd);

            writePhotoToByte(photoPath, dos);
            deletePhoto(photoPath, db);

            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary);

            writeFormData("photoId_" + i, String.valueOf(photoId), dos);

            writeFormData("entranceId_" + i, String.valueOf(entranceId), dos);

            writeFormData("employeeId_" + i, String.valueOf(employeeId), dos);

            writeFormData("eventId_" + i, String.valueOf(eventId), dos);

            i++;
        }
        cursor.close();

        writeFormData("login", Settings.getLogin(), dos);
        writeFormData("password", Settings.getPassword(), dos);
        writeFormData("imei", Settings.getIMEI(), dos);

        dos.writeBytes(twoHyphens + lineEnd);
        dos.flush();
        dos.close();

        String response = "";
        try {

            DataInputStream inStream = new DataInputStream(conn.getInputStream());
            String responseFromServer = "";

            while ((responseFromServer = inStream.readLine()) != null) {

                Log.d("WebConnection", "Server Response: " + responseFromServer);
                response += responseFromServer;

            }

            inStream.close();

        } catch (IOException ioex) {
            Log.e("WebConnection", "error: " + ioex.getMessage(), ioex);

        } finally {
            db.close();
        }
        return response;

    }

    private static void writeFormData(String dataName, String data , DataOutputStream dos)
            throws IOException {
        dos.writeBytes(lineEnd);
        dos.writeBytes("Content-Disposition: form-data; name=\"" + dataName + "\"" + lineEnd);
        dos.writeBytes(lineEnd);
        dos.writeBytes(data + lineEnd);
        dos.writeBytes(twoHyphens + boundary);
    }

    private static void deletePhoto(String photoPath, SQLiteDatabase db){
        new File(photoPath).delete();
        String[] params = new String[]{photoPath};
        db.delete("photo", WHERE_PHOTO_PATH, params);
    }

    private static void writePhotoToByte(String photoPath, DataOutputStream dos)
            throws IOException {
        FileInputStream fileInputStream = new FileInputStream(new File(photoPath));

        int bytesAvailable = fileInputStream.available();
        int bufferSize = Math.min(bytesAvailable, maxBufferSize);
        byte[] buffer = new byte[bufferSize];
        int bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        while (bytesRead > 0) {

            dos.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        }
    }

    public static boolean hasConnection(Context context)
    {

        if(Settings.isOffline()){
            return false;
        }

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                return true;
            }
        }

        return false;
    }
}
