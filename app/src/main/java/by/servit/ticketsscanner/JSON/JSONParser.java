package by.servit.ticketsscanner.JSON;


import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import by.servit.ticketsscanner.Data.DBHelper;

import static by.servit.ticketsscanner.Data.DBEssence.Pass.DB_BAD_BARCODE;
import static by.servit.ticketsscanner.Data.DBEssence.Pass.DB_BAD_DATE;
import static by.servit.ticketsscanner.Data.DBEssence.Pass.DB_BAD_EMPLOYEE_ID;
import static by.servit.ticketsscanner.Data.DBEssence.Pass.DB_BAD_EVENT_ID;
import static by.servit.ticketsscanner.Data.DBEssence.Pass.DB_BAD_ID;
import static by.servit.ticketsscanner.Data.DBEssence.Pass.DB_DEVICE_ID;
import static by.servit.ticketsscanner.Data.DBEssence.Pass.DB_DIRECTION;
import static by.servit.ticketsscanner.Data.DBEssence.Pass.DB_EMPLOYEE_ID;
import static by.servit.ticketsscanner.Data.DBEssence.Pass.DB_ENTRY_DATE;
import static by.servit.ticketsscanner.Data.DBEssence.Pass.DB_EVENT_ID;
import static by.servit.ticketsscanner.Data.DBEssence.Pass.DB_ID;
import static by.servit.ticketsscanner.Data.DBEssence.Pass.DB_ISSERVER;
import static by.servit.ticketsscanner.Data.DBEssence.Pass.DB_RESULT;
import static by.servit.ticketsscanner.Data.DBEssence.Pass.DB_TICKET_ID;

public class JSONParser {

    public static ContentValues getEmployeeContentValues(JSONArray jsonArray, int index)
            throws JSONException {

        JSONObject jsonObject = jsonArray.getJSONObject(index);
        ContentValues contentValues = new ContentValues();
        contentValues.put("_id", jsonObject.optInt("id"));
        contentValues.put("full_name", jsonObject.optString("full_name"));
        contentValues.put("active", jsonObject.optInt("active"));
        contentValues.put("allowed_outputs", jsonObject.optInt("allowed_outputs"));
        contentValues.put("type", jsonObject.optString("type"));
        contentValues.put("login", jsonObject.optString("login"));
        contentValues.put("password", jsonObject.optString("password", "\"\""));
        contentValues.put("operator_id", jsonObject.optInt("operator_id"));
        contentValues.put("organizer_id", jsonObject.optInt("organizer_id"));
        contentValues.put("platform_id", jsonObject.optInt("platform_id"));

        return contentValues;
    }

    public static ContentValues getEventContentValues(JSONArray jsonArray, int index)
            throws JSONException, ParseException {


        JSONObject jsonObject = jsonArray.getJSONObject(index);
        ContentValues contentValues = new ContentValues();
        contentValues.put("_id", jsonObject.optString("id"));
        contentValues.put("name", jsonObject.optString("name"));

        Date parsedDate = DBHelper.dateFormat.parse(jsonObject.optString("date_event"));
        Timestamp timestamp = new Timestamp(parsedDate.getTime());
        contentValues.put("date_event", timestamp.getTime() / 1000 + 3*60*60);

        parsedDate = DBHelper.dateFormat.parse(jsonObject.optString("date_start_enter"));
        timestamp = new java.sql.Timestamp(parsedDate.getTime());
        contentValues.put("date_start_enter", timestamp.getTime() / 1000 + 3*60*60);

        parsedDate = DBHelper.dateFormat.parse(jsonObject.optString("date_end_enter"));
        timestamp = new java.sql.Timestamp(parsedDate.getTime());
        contentValues.put("date_end_enter", timestamp.getTime() / 1000 + 3*60*60);

        contentValues.put("platform_id", jsonObject.optInt("platform_id"));
        contentValues.put("organizer_id", jsonObject.optInt("organizer_id"));
        contentValues.put("can_choose", jsonObject.optInt("can_choose"));

        return contentValues;
    }

    public static ContentValues getTicketsContentValues(JSONArray jsonArray, int index)
            throws JSONException, ParseException {

        JSONObject jsonObject = jsonArray.getJSONObject(index);
        ContentValues contentValues = new ContentValues();
        contentValues.put("_id", jsonObject.optString("id"));

        Date parsedDate = DBHelper.dateFormat.parse(jsonObject.optString("date_upload"));
        Timestamp timestamp = new Timestamp(parsedDate.getTime());
        contentValues.put("date_upload",  timestamp.getTime() / 1000 + 3*60*60);

        contentValues.put("operator_id", jsonObject.optInt("operator_id"));
        contentValues.put("event_id", jsonObject.optInt("event_id"));
        contentValues.put("barcode", jsonObject.optString("barcode"));
        contentValues.put("sector", jsonObject.optString("sector"));
        contentValues.put("row", jsonObject.optInt("row"));
        contentValues.put("place", jsonObject.optInt("place"));
        contentValues.put("no_place", jsonObject.optInt("no_place"));
        contentValues.put("active", jsonObject.optInt("active"));
        contentValues.put("transactional", jsonObject.optInt("transactional"));

        return contentValues;
    }

    public static ContentValues getPassContentValues(JSONArray jsonArray, int index)
            throws JSONException, ParseException {

        JSONObject jsonObject = jsonArray.getJSONObject(index);
        ContentValues contentValues = new ContentValues();
        contentValues.put("_id", jsonObject.optString("id"));

        Date parsedDate = DBHelper.dateFormat.parse(jsonObject.optString("entry_date"));
        Timestamp timestamp = new Timestamp(parsedDate.getTime());
        contentValues.put("entry_date",  timestamp.getTime() / 1000 + 3*60*60);

        contentValues.put("device_id", jsonObject.optInt("device_id"));
        contentValues.put("ticket_id", jsonObject.optInt("ticket_id"));
        contentValues.put("employee_id", jsonObject.optInt("employee_id"));
        contentValues.put("event_id", jsonObject.optInt("event_id"));
        contentValues.put("direction", jsonObject.optInt("direction"));
        contentValues.put("result", jsonObject.optString("result"));

        return contentValues;
    }

    public static JSONObject getPassesNotInServerJson(Cursor cursor) throws JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("id", cursor.getInt(DB_ID));
        jsonObject.put("device_id", cursor.getInt(DB_DEVICE_ID));
        jsonObject.put("ticked_id", cursor.getInt(DB_TICKET_ID));
        jsonObject.put("employee_id", cursor.getInt(DB_EMPLOYEE_ID));
        jsonObject.put("event_id", cursor.getInt(DB_EVENT_ID));
        jsonObject.put("entry_date", cursor.getLong(DB_ENTRY_DATE));
        jsonObject.put("direction", DBHelper.getBoolean(cursor, DB_DIRECTION));
        jsonObject.put("result", cursor.getString(DB_RESULT));
        jsonObject.put("isServer", DBHelper.getBoolean(cursor, DB_ISSERVER));

        return jsonObject;
    }

    public static JSONObject getBadPassesNotInServerJson(Cursor cursor) throws JSONException{

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("id", cursor.getInt(DB_BAD_ID));
        jsonObject.put("barcode", cursor.getInt(DB_BAD_BARCODE));
        jsonObject.put("employee_id", cursor.getInt(DB_BAD_EMPLOYEE_ID));
        jsonObject.put("event_id", cursor.getInt(DB_BAD_EVENT_ID));
        jsonObject.put("entry_date", cursor.getLong(DB_BAD_DATE));


        return jsonObject;
    }
}
