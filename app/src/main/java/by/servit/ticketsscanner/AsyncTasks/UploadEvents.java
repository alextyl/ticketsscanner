package by.servit.ticketsscanner.AsyncTasks;


import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.MalformedURLException;
import java.text.ParseException;

import by.servit.ticketsscanner.ChooseEventActivity;
import by.servit.ticketsscanner.Data.DBHelper;
import by.servit.ticketsscanner.Data.DBInstance.Events;
import by.servit.ticketsscanner.JSON.JSONParser;
import by.servit.ticketsscanner.WEB.WebConnection;

import static by.servit.ticketsscanner.Data.DBHelper.DB_NAME;
import static by.servit.ticketsscanner.WEB.WebConnection.HOST;

public class UploadEvents extends AsyncTask <Void, Void, Void >{

    private static final String URL_UPLOAD_EVENTS = HOST + "/SendEvents.php";

    private ChooseEventActivity mActivity;

    public UploadEvents(ChooseEventActivity Activity){
        this.mActivity = Activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mActivity.visibilityUpdateDB(true);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        DBHelper dbHelper =  new DBHelper(mActivity);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if(WebConnection.hasConnection(mActivity)){
            try { // TODO: сделать по нажатию
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            takeEventsFromServer(db);
        }
        Events.getInstance().uploadEvents(mActivity);
        db.close();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        mActivity.setEventsList();
        mActivity.visibilityUpdateDB(false);
    }

    public static void takeEventsFromServer(SQLiteDatabase db){
        String response = "";
        try {
            WebConnection webConnection = new WebConnection(URL_UPLOAD_EVENTS);
            response = webConnection.getResponse();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return;
        }

        if (response == null) return;

        JSONArray responseJson;
        try {
            responseJson = new JSONArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        db.delete("event", null, null);

        for(int i = 0; i < responseJson.length(); i++){
            ContentValues contentValues = null;
            try {
                contentValues = JSONParser.getEventContentValues(responseJson, i);
            } catch (JSONException | ParseException e) {
                e.printStackTrace();
                return;
            }

            Log.d(DB_NAME, contentValues.toString());
            db.insert("event", null, contentValues);
        }
    }
}
