package by.servit.ticketsscanner.AsyncTasks;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

import by.servit.ticketsscanner.Data.DBEssence.Pass;
import by.servit.ticketsscanner.Data.DBHelper;
import by.servit.ticketsscanner.Data.DBInstance.Events;
import by.servit.ticketsscanner.Data.DBInstance.Passes;
import by.servit.ticketsscanner.Data.DBInstance.User;
import by.servit.ticketsscanner.JSON.JSONParser;
import by.servit.ticketsscanner.R;
import by.servit.ticketsscanner.TicketsScannerActivity;
import by.servit.ticketsscanner.WEB.WebConnection;

import static by.servit.ticketsscanner.Data.DBHelper.DB_NAME;
import static by.servit.ticketsscanner.Data.DBHelper.WHERE_BARCODE_EVENT;
import static by.servit.ticketsscanner.Data.DBHelper.WHERE_ID;
import static by.servit.ticketsscanner.WEB.WebConnection.HOST;


public class ReadTicket extends AsyncTask <String, Void, Boolean>{

    private static final String URL_FIND_TICKET = HOST + "/ReadTicket.php";

    private TicketsScannerActivity mActivity;
    private boolean isCurrentTime = true;
    private long mPassId;

    public ReadTicket(TicketsScannerActivity ticketsScannerActivity){
        mActivity = ticketsScannerActivity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mActivity.setAsyncTask(true);
        checkedCurrentTime();
    }

    @Override
    protected Boolean doInBackground(String... barcode) {
        if(!isCurrentTime){
            return false;
        }
        DBHelper dbHelper =  new DBHelper(mActivity);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if(findTicket(db, barcode[0], mActivity)){
            if (WebConnection.hasConnection(mActivity)){
                setPassOnServer(barcode[0], Events.getInstance().getCurrentEventId(), db);
            }
            db.close();
            return true;
        } else if (WebConnection.hasConnection(mActivity)){
            return findTicketFromServer(barcode[0], Events.getInstance().getCurrentEventId(), db);
        }
        insertBadPass(db, barcode[0]);
        db.close();
        return false;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        mActivity.setLastPassesList( DBHelper.timeFormat.format(new Date())+ " " +
                        mActivity.getLastText() + " " +
                        (!aBoolean ? "Билета нет" : (Passes.getInstance().getRetryPass() > 1 ?
                                "Повтор" : "Билет есть")), true);
        if(!isCurrentTime){
            mActivity.setAsyncTask(false);
            return;
        }
        mActivity.setPassesList();
        Vibrator vibrator = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
        if(aBoolean){
            if(Passes.getInstance().getRetryPass() > 1){
                retryTicket(vibrator);
                return;
            }
           goodTicket(vibrator);
        } else {
            badTicket(vibrator);
        }
        mActivity.setAsyncTask(false);
    }

    private  void goodTicket(Vibrator vibrator){
        Toast.makeText(mActivity, "Билет есть", Toast.LENGTH_SHORT).show();
        mActivity.stroboscope(500, 2);
        mActivity.setStatusTextColorGood();
        mActivity.getBarcodeView().setStatusText(mActivity.getLastText() + " Билет есть");
        vibrator.vibrate(500);
        mActivity.playSound(R.raw.good);
        mActivity.getPhotoButton().setVisibility(View.GONE);
    }

    private  void badTicket(Vibrator vibrator){
        Toast.makeText(mActivity, "Билета нет", Toast.LENGTH_SHORT).show();
        mActivity.getBarcodeView().setStatusText(mActivity.getLastText() + " Билета нет");
        mActivity.stroboscope(200, 4);
        vibrator.vibrate(new long[]{0, 300, 100, 300, 100, 300}, -1);
        mActivity.setStatusTextColorBad();
        mActivity.playSound(R.raw.bad);
        mActivity.getPhotoButton().setVisibility(View.VISIBLE);
    }

    private  void retryTicket(Vibrator vibrator){
        Toast.makeText(mActivity, "Повторный проход: " +
                Passes.getInstance().getRetryPass() +
                " попытка", Toast.LENGTH_SHORT).show();
        vibrator.vibrate(1000);
        mActivity.getBarcodeView().setStatusText(mActivity.getLastText() +
                " Повтор");
        mActivity.setAsyncTask(false);
        mActivity.stroboscope(500, 2);
        mActivity.setStatusTextColorRepeat();
        mActivity.playSound(R.raw.retry);
        mActivity.getPhotoButton().setVisibility(View.VISIBLE);
    }

    private void insertBadPass(SQLiteDatabase db, String barcode){
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", System.currentTimeMillis() / 1000);
        contentValues.put("event_id", Events.getInstance().getCurrentEventId());
        contentValues.put("barcode", barcode);
        contentValues.put("employee_id", User.getInstance().getId());
        db.insert("bad_pass", null, contentValues);
    }



    private boolean checkedCurrentTime(){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long time = timestamp.getTime() / 1000;

        if(time < Events.getInstance().getCurrentEventDateStartEnter() - 3600 * 3){
            isCurrentTime = false;
            Toast.makeText(mActivity, "Мероприятие ещё не началось", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(time > Events.getInstance().getCurrentgetDateEndEnter() - 3600 * 3){
            isCurrentTime = false;
            Toast.makeText(mActivity, "Мероприятие уже закончилось", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private boolean findTicket(SQLiteDatabase db, String barcode, Context context){
        String[] selectionArgs = new String[]{barcode,
                String.valueOf(Events.getInstance().getCurrentEventId())};
        Cursor cursor = db.query("ticket", null, WHERE_BARCODE_EVENT, selectionArgs,
                null, null, null);
        if(cursor.getCount() > 0){
            cursor.moveToFirst();
            setPassOnDB(db, cursor.getInt(0), context, false);
            Passes.getInstance().uploadPasses(mActivity, cursor.getInt(0),
                    Events.getInstance().getCurrentEventId());
            cursor.close();
            return true;
        }
        Passes.getInstance().uploadPasses(mActivity, -1,
                Events.getInstance().getCurrentEventId());
        return false;
    }

    private void setPassOnDB(SQLiteDatabase db, int ticketId, Context context, Boolean fromServer){
        ContentValues contentValues = new ContentValues();
        contentValues.put("ticket_id", ticketId);
        contentValues.put("employee_id", User.getInstance().getId());
        contentValues.put("event_id", Events.getInstance().getCurrentEventId());
        contentValues.put("entry_date", new Timestamp(System.currentTimeMillis()).getTime() / 1000);
        contentValues.put("direction", Pass.checkDirection(ticketId,
                Events.getInstance().getCurrentEventId(), context));
        if(fromServer){
            contentValues.put("inServer", Pass.ON_SERVER);
        } else {
            contentValues.put("inServer", Pass.NOT_ON_SERVER);
        }
        contentValues.put("result", "good");
        Log.d(DB_NAME, contentValues.toString());
        mPassId = db.insert("pass", null, contentValues);
        Passes.getInstance().uploadPasses(context, ticketId,
                Events.getInstance().getCurrentEventId());
    }

    private void setPassOnServer(String barcode, int eventId, SQLiteDatabase db){
        JSONObject json = new JSONObject();
        try {
            json.put("barcode", barcode);
            json.put("eventId", eventId);
            json.put("employeeId", User.getInstance().getId());
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        WebConnection webConnection;
        String response = "";
        try {
            webConnection = new WebConnection(URL_FIND_TICKET, json);
            response = webConnection.getResponse();
        } catch (MalformedURLException e) {
            return;
        }

        JSONObject jsonResponse;

        try {
            jsonResponse = new JSONObject(response);
            if(jsonResponse.getInt("readTicket") == 1){
                updatePassLikeOnServer(db, mPassId);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void updatePassLikeOnServer(SQLiteDatabase db, long passId){
        ContentValues contentValues = new ContentValues();
        contentValues.put("inServer", Pass.ON_SERVER);
        db.update("pass", contentValues, WHERE_ID, new String[]{ String.valueOf(passId) });
    }

    private boolean findTicketFromServer(String barcode, int eventId, SQLiteDatabase db){

        JSONObject json = new JSONObject();
        try {
            json.put("barcode", barcode);
            json.put("eventId", eventId);
            json.put("employeeId", User.getInstance().getId());
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }

        WebConnection webConnection;
        try {
            webConnection = new WebConnection(URL_FIND_TICKET, json);
        } catch (MalformedURLException e) {
            return false;
        }

        String response = webConnection.getResponse();

        if(response == null) return false;

        JSONObject jsonResponse;
        try {
            jsonResponse = new JSONObject(response);
            if(jsonResponse.getInt("readTicket") == 1){
                ContentValues contentValues =
                        JSONParser.getTicketsContentValues(jsonResponse.getJSONArray("result"), 0);
                db.insert("ticket", null, contentValues);
                setPassOnDB(db, jsonResponse.getJSONArray("result").getJSONObject(0)
                        .getInt("id"), mActivity, true);
                return true;
            } else {
                return false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }

    }

}
