package by.servit.ticketsscanner.AsyncTasks;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.MalformedURLException;
import java.text.ParseException;

import by.servit.ticketsscanner.Data.DBEssence.Pass;
import by.servit.ticketsscanner.Data.DBHelper;
import by.servit.ticketsscanner.JSON.JSONParser;
import by.servit.ticketsscanner.WEB.WebConnection;

import static by.servit.ticketsscanner.Data.DBHelper.DB_NAME;
import static by.servit.ticketsscanner.Data.DBHelper.WHERE_NOT_IN_SERVER;
import static by.servit.ticketsscanner.WEB.WebConnection.HOST;


public class UploadPasses extends AsyncTask <Void, Void, Boolean> {

    private static final String URL_UPLOAD_PASSES = HOST + "/SendPasses.php";
    private static final String URL_UPLOAD_BAD_PASSES = HOST + "/SendBadPasses.php";

    private Context mContext;

    public UploadPasses(Context context){
        mContext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        DBHelper dbHelper =  new DBHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if(WebConnection.hasConnection(mContext)){
            try {
                uploadPasses(getJsonArrayPasses(db), db);
                uploadBadPasses(getJsonArrayBadPasses(db), db);
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }
        db.close();
        return true;
    }

    public static JSONArray getJsonArrayPasses(SQLiteDatabase db) throws JSONException {

        Cursor cursor = db.query("pass", null, WHERE_NOT_IN_SERVER, null, null, null, null);
        JSONArray jsonArray = new JSONArray();

        if(cursor.getCount() > 0){
            for(int i = 0; i < cursor.getCount(); i++){
                cursor.moveToPosition(i);
                jsonArray.put(JSONParser.getPassesNotInServerJson(cursor));
            }

        }

        cursor.close();
        Log.d(DB_NAME, jsonArray.toString());
        return jsonArray;
    }

    public static JSONArray getJsonArrayBadPasses(SQLiteDatabase db) throws JSONException {

        Cursor cursor = db.query("bad_pass", null, null, null, null, null, null);
        JSONArray jsonArray = new JSONArray();

        if(cursor.getCount() > 0){
            for(int i = 0; i < cursor.getCount(); i++){
                cursor.moveToPosition(i);
                jsonArray.put(JSONParser.getBadPassesNotInServerJson(cursor));
            }

        }

        cursor.close();
        Log.d(DB_NAME, jsonArray.toString());
        return jsonArray;
    }

    public static void uploadPasses(JSONArray jsonArray, SQLiteDatabase db){
        String response = "";
        try {
            WebConnection webConnection = new WebConnection(URL_UPLOAD_PASSES, jsonArray);
            response = webConnection.getResponse();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return;
        }

        if (response == null) return;

        JSONArray responseJson;
        try {
            responseJson = new JSONArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        db.delete("pass", null, null);

        for(int i = 0; i < responseJson.length(); i++){
            ContentValues contentValues = null;
            try {
                contentValues = JSONParser.getPassContentValues(responseJson, i);
            } catch (JSONException | ParseException e) {
                e.printStackTrace();
                return;
            }

            contentValues.put("inServer", Pass.ON_SERVER);
            Log.d(DB_NAME, contentValues.toString());
            db.insert("pass", null, contentValues);
        }
    }

    public static void uploadBadPasses(JSONArray jsonArray, SQLiteDatabase db){
        try {
            WebConnection webConnection = new WebConnection(URL_UPLOAD_BAD_PASSES, jsonArray);
            webConnection.getResponse();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return;
        }

        db.delete("bad_pass", null, null);

    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
    }
}
