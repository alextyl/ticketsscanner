package by.servit.ticketsscanner.AsyncTasks;

import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;

import by.servit.ticketsscanner.ChooseEventActivity;
import by.servit.ticketsscanner.Data.DBHelper;
import by.servit.ticketsscanner.Data.DBInstance.Events;
import by.servit.ticketsscanner.Data.Settings;
import by.servit.ticketsscanner.WEB.WebConnection;

import static by.servit.ticketsscanner.AsyncTasks.UploadTickets.ALL_EVENTS_TICKETS;

public class FullSynchronization extends AsyncTask <Void, Void, Boolean> {

    private ChooseEventActivity mContext;

    public FullSynchronization(ChooseEventActivity context){
        this.mContext = context;
    }

    @Override
    protected void onPreExecute() {
        mContext.visibilityUpdateDB(true);
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        DBHelper dbHelper =  new DBHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        if(!WebConnection.hasConnection(mContext)){
            return false;
        }

        if(Settings.checkSynchronizationSettings()){
            return false;
        }

        if(Settings.isDownloadUsers()){
            CreateDB.takeUsersFromServer(db);
        }

        if(Settings.isDownloadEvents()){
            UploadEvents.takeEventsFromServer(db);
        }

        if(Settings.isDownloadTickets()){
            UploadTickets.takeTicketsFromServer(db, ALL_EVENTS_TICKETS);
        }


        if(Settings.isUploadPhoto()){
            try {
                WebConnection.uploadPhotos(mContext);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(Settings.isDownloadPasses()){
            try {
                UploadPasses.uploadPasses(UploadPasses.getJsonArrayPasses(db), db);
                UploadPasses.uploadBadPasses(UploadPasses.getJsonArrayBadPasses(db), db);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        mContext.visibilityUpdateDB(false);
        if(aBoolean){
            Toast.makeText(mContext, "Конец синхронизации", Toast.LENGTH_SHORT).show();
            Events.getInstance().uploadEvents(mContext);
            mContext.setEventsList();
        } else {
            Toast.makeText(mContext, "Ошибка синхронизации", Toast.LENGTH_SHORT).show();
        }
    }

}
