package by.servit.ticketsscanner.AsyncTasks;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.MalformedURLException;

import by.servit.ticketsscanner.Data.DBHelper;
import by.servit.ticketsscanner.JSON.JSONParser;
import by.servit.ticketsscanner.LoginActivity;
import by.servit.ticketsscanner.WEB.WebConnection;


import static by.servit.ticketsscanner.Data.DBHelper.DB_NAME;
import static by.servit.ticketsscanner.WEB.WebConnection.HOST;

public class CreateDB extends AsyncTask <Void, Void, Void>{

    private static final String URL_UPLOAD_USERS = HOST + "/SendUsers.php";

    private LoginActivity mActivity;

    public CreateDB(LoginActivity Activity){
        this.mActivity = Activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mActivity.visibilityUpdateDB(true);
    }

    @Override
    protected Void doInBackground(Void[] voids) {

        DBHelper dbHelper =  new DBHelper(mActivity);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if(WebConnection.hasConnection(mActivity)){
            try { // TODO: сделать по нажатию
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            takeUsersFromServer(db);
        }
        debugInsert(db);
        db.close();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        mActivity.visibilityUpdateDB(false);
    }

    public static void takeUsersFromServer(SQLiteDatabase db){
        String response = "";
        try {
            WebConnection webConnection = new WebConnection(URL_UPLOAD_USERS);
            response = webConnection.getResponse();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return;
        }

        if (response == null) return;

        JSONArray responseJson;
        try {
            responseJson = new JSONArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        db.delete("employee", null, null);

        for(int i = 0; i < responseJson.length(); i++){
            ContentValues contentValues = null;
            try {
                contentValues = JSONParser.getEmployeeContentValues(responseJson, i);
            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }
            Log.d(DB_NAME, contentValues.toString());
            db.insert("employee", null, contentValues);
        }
    }

    private void debugInsert(SQLiteDatabase db){
        // db.execSQL("insert into employee (full_name, active, allowed_outputs, type, login, password) values (\"Иванов Иван Иванович\", 1, 1, \"Чеклер\", \"3901098234878073\", \"6889\");");
        // db.execSQL("insert into employee (full_name, active, allowed_outputs, type, login, password) values (\"Екатерина Баранова\", 1, 1, \"Мурмка\", \"4810833014275\", \"16\");");
        // db.execSQL("insert into event (name, date_event) values (\"мероприятие\", \"1541894400\");");
        // db.execSQL("insert into event (name, date_event) values (\"позднее\", \"1510358400\");");
      //   db.execSQL("insert into ticket (barcode, event_id) values (\"4810833014275\", \"1\");");
     //   db.execSQL("insert into ticket (barcode, event_id) values (\"4811543003283\", \"1\");");
      //  db.execSQL("insert into ticket (barcode, event_id) values (\"4810256017310\", \"7\");");
       // db.execSQL("insert into event (name, date_event) values (\"мероприятие 2\", \"1513251024000\");");
      /*  Cursor cursor = db.query("pass", null, null, null,
                null, null, null);
        cursor.moveToFirst();

        for (int i = 0; i < cursor.getCount(); i++){
            Log.d("lesha", "_________________________");
            Log.d("lesha", cursor.getInt(0) + "");
            Log.d("lesha", cursor.getInt(2) + "");
            Log.d("lesha", cursor.getInt(7) + "");
        }

        cursor.close();*/
        /*Cursor cursor = db.query("photo", null, null, null, null, null, null , null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++){
            Log.d("lesha", "_________________________");
            Log.d("lesha", cursor.getInt(0) + "");
            Log.d("lesha", cursor.getString(1) + "");
            Log.d("lesha", cursor.getInt(2) + "");
            Log.d("lesha", cursor.getString(3) + "");
            Log.d("lesha", cursor.getInt(4) + "");
            cursor.moveToNext();
        }
        cursor.close();
        db.delete("photo", null, null);*/
    }
}
