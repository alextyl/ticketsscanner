package by.servit.ticketsscanner.AsyncTasks;


import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.text.ParseException;

import by.servit.ticketsscanner.ChooseEventActivity;
import by.servit.ticketsscanner.Data.DBHelper;
import by.servit.ticketsscanner.JSON.JSONParser;
import by.servit.ticketsscanner.TicketsScannerActivity;
import by.servit.ticketsscanner.WEB.WebConnection;

import static by.servit.ticketsscanner.Data.DBHelper.DB_NAME;
import static by.servit.ticketsscanner.Data.DBHelper.WHERE_EVENT_ID;
import static by.servit.ticketsscanner.WEB.WebConnection.HOST;

public class UploadTickets extends AsyncTask <Integer, Void, Void>{

    private static final String URL_UPLOAD_TICKETS = HOST + "/SendTickets.php";
    public static final int ALL_EVENTS_TICKETS = -1;

    private ChooseEventActivity mActivity;

    public UploadTickets(ChooseEventActivity chooseEventActivity){
        mActivity = chooseEventActivity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mActivity.visibilityUpdateDB(true);
    }

    @Override
    protected Void doInBackground(Integer... integers) {
        DBHelper dbHelper =  new DBHelper(mActivity);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if(WebConnection.hasConnection(mActivity)){
            takeTicketsFromServer(db, integers[0]);
        }
        db.close();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        mActivity.visibilityUpdateDB(false);
        Intent intent = new Intent(mActivity, TicketsScannerActivity.class);
        mActivity.startActivity(intent);
    }

    public static void takeTicketsFromServer(SQLiteDatabase db, int eventId){

        JSONObject json = new JSONObject();
        try {
            json.put("eventId", eventId);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        String response = "";
        try {
            WebConnection webConnection = new WebConnection(URL_UPLOAD_TICKETS, json);
            response = webConnection.getResponse();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return;
        }

        if (response == null) return;

        JSONArray responseJson;
        try {
            responseJson = new JSONArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        if(eventId == ALL_EVENTS_TICKETS){
            db.delete("ticket", null, null);
        } else {
            db.delete("ticket", WHERE_EVENT_ID, new String[]{String.valueOf(eventId)});
        }

        for(int i = 0; i < responseJson.length(); i++){
            ContentValues contentValues;
            try {
                contentValues = JSONParser.getTicketsContentValues(responseJson, i);
            } catch (JSONException | ParseException e) {
                e.printStackTrace();
                return;
            }
            Log.d(DB_NAME, contentValues.toString());
            db.insert("ticket", null, contentValues);
        }
    }
}
