package by.servit.ticketsscanner;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import by.servit.ticketsscanner.Alerts.SettingsDialog;
import by.servit.ticketsscanner.AsyncTasks.FullSynchronization;
import by.servit.ticketsscanner.AsyncTasks.UploadEvents;
import by.servit.ticketsscanner.AsyncTasks.UploadTickets;
import by.servit.ticketsscanner.Data.DBEssence.Event;
import by.servit.ticketsscanner.Data.DBInstance.Events;
import by.servit.ticketsscanner.Data.Settings;

public class ChooseEventActivity extends AppCompatActivity {

    private ListView mEventsList;
    private ProgressBar mUpdateProgressBar;
    private TextView mUpdateTextView;
    private Toolbar mToolbar;
    private Menu mMainMeu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(android.R.style.Theme_Holo_NoActionBar);

        setContentView(R.layout.activity_choose_event);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("События");
        setSupportActionBar(mToolbar);

        mEventsList = (ListView) findViewById(R.id.events_list);
        mEventsList.setOnItemClickListener(clickOnEventsList());

        mUpdateProgressBar = (ProgressBar) findViewById(R.id.update_event_progressBar);
        mUpdateTextView = (TextView) findViewById(R.id.update_event_textView);

        new UploadEvents(this).execute();

    }

    @Override
    public void onBackPressed() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }

    private AdapterView.OnItemClickListener clickOnEventsList(){
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Events.getInstance().setCurrentEventId(
                        Events.getInstance().getEvents().get(i).getId());
                new UploadTickets(ChooseEventActivity.this).execute(Events.getInstance()
                        .getCurrentEventId());
            }
        };
    }

    public void visibilityUpdateDB(boolean isUpdate){
        if(isUpdate){
            mEventsList.setVisibility(View.GONE);
            mUpdateProgressBar.setVisibility(View.VISIBLE);
            mUpdateTextView.setVisibility(View.VISIBLE);
        } else {
            mEventsList.setVisibility(View.VISIBLE);
            mUpdateProgressBar.setVisibility(View.GONE);
            mUpdateTextView.setVisibility(View.GONE);
        }
    }

    public void setEventsList(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, Events.getInstance().getEventsNames());
        mEventsList.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        mMainMeu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.sync_menu: {
                new FullSynchronization(ChooseEventActivity.this).execute();
                return true;
            }
            case R.id.logout_menu: {
                SharedPreferences sp = this.getSharedPreferences("SETTINGS",Context.MODE_PRIVATE);
                SharedPreferences.Editor ed = sp.edit();
                ed.putString(Settings.LOGIN, "");
                ed.putString(Settings.PASSWORD, "");
                ed.commit();
                finish();
            }
            case R.id.settings_menu: {
                new SettingsDialog(this).mDialogBuilder.show();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}


